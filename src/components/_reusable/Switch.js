import React from 'react';
import { Platform, StyleSheet, Switch as ReactNativeSwitch, View } from 'react-native';


const Switch = ({
  value,
  onValueChange,
  children,
  style,
  disabled = false,
  trackColorOn = '#FF4C9A',
  trackColorOff = 'gray'
}) => (
  <View style={{ ...styles.row, ...style }}>
    <View style={styles.leftColumn}>{children}</View>
    <View style={styles.rightColumn}>
      <ReactNativeSwitch
        onValueChange={onValueChange}
        value={value}
        style={{ opacity: Platform.OS === 'android' && disabled ? 0.5 : 1 }}
        trackColor={{ false: trackColorOff, true: trackColorOn }}
        thumbColor="#F1F1F1"
        disabled={disabled}
      />
    </View>
  </View>
);

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignSelf: 'stretch'
  },
  leftColumn: { flexShrink: 1 },
  rightColumn: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexShrink: 0
  },
  lock: { marginHorizontal: 5 }
});

export { Switch };
