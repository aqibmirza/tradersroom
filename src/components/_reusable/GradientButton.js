import React from 'react';
import { StyleSheet } from 'react-native';
import { Button } from './Button';
import { Gradient } from './Gradient';
import { colors } from '../../designSystem';

const GradientButton = ({ onPress, children, style, disabled, testID }) => (
  <Gradient
    colors={['#EF7B40', '#FF4B9B']}
    style={{
      width: '100%',
      borderRadius: 5,
      opacity: disabled ? 0.4 : 1,
      ...style
    }}
  >
    <Button
      testID={testID}
      onPress={onPress}
      style={styles.buttonStyle}
      textStyle={styles.buttonTextStyle}
      disabled={disabled}
    >
      {children}
    </Button>
  </Gradient>
);

const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: colors.transparent,
    borderColor: colors.transparent,
    marginTop: 0
  },
  buttonTextStyle: {
    paddingTop: 15,
    paddingBottom: 15
  }
});

export { GradientButton };
