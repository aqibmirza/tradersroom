import React from 'react';
import { P1 } from '../../designSystem';

const InputLabel = props => (
  <P1 opacity={1} style={{ marginBottom: 5 }}>
    {props.label}
  </P1>
);

export { InputLabel };
