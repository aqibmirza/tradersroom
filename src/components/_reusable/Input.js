import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import {InputLabel} from './InputLabel';
import {ErrorMessage} from './ErrorMessage';
import {colors} from '../../designSystem';

class Input extends React.Component {
  focus = () => {
    this.input.focus();
  };

  blur = () => {
    this.input.blur();
  };

  render() {
    const {
      placeholder,
      value,
      onChangeText,
      onFocus,
      secureTextEntry,
      keyboardType,
      autoFocus,
      autoCapitalize,
      autoCompleteType,
      onSubmitEditing,
      editable = true,
      returnKeyType = 'next',
      label,
      error,
      styleInput = {},
      selectTextOnFocus = false,
      testID,
      maxLength,
      icon,
    } = this.props;
    const {inputStyle} = styles;
    const marginTop = label ? 2 : 10;
    const marginBottom = error ? 0 : 10;
    return (
      <View style={{flexDirection: icon ? 'row' : 'column'}}>
        {label && <InputLabel label={label} />}
        {icon && (
          <View
            style={{
              position: 'absolute',
              zIndex: 1,
              justifyContent: 'center',
              alignSelf: 'center',
              opacity: 1,
              backgroundColor: 'white',
              marginLeft: 10,
              width: 20,
            }}>
            {icon}
          </View>
        )}
        <TextInput
          value={value}
          onChangeText={onChangeText}
          onFocus={onFocus}
          returnKeyType={returnKeyType}
          ref={ref => {
            this.input = ref;
          }}
          style={[
            {
              ...inputStyle,
              backgroundColor: `${editable ? colors.white : colors.brightGray}`,
              marginTop,
              marginBottom,
              ...styleInput,
            },
            icon && {flex: 1},
            {paddingLeft: icon ? 35 : 10},
            error && {borderWidth: 1, borderColor: colors.red},
          ]}
          onSubmitEditing={onSubmitEditing}
          placeholder={placeholder}
          autoCorrect={false}
          secureTextEntry={secureTextEntry}
          keyboardType={keyboardType || 'default'}
          placeholderTextColor={colors.greyDark}
          autoFocus={autoFocus}
          autoCapitalize={autoCapitalize || 'sentences'}
          selectionColor="#FF4B9B"
          editable={editable}
          selectTextOnFocus={selectTextOnFocus}
          autoCompleteType={autoCompleteType || 'off'}
          testID={testID}
          maxLength={maxLength}
        />
        {error && <ErrorMessage description={error} />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputStyle: {
    alignSelf: 'stretch',
    color: colors.greyDark,
    fontSize: 16,
    // backgroundColor: colors.white,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.greyDark,
    minHeight: 48,
  },
});

export {Input};
