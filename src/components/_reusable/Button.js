import React from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, StyleSheet} from 'react-native';
import {H3, colors} from '../../designSystem';

const Button = ({
  onPress,
  inverted,
  children,
  style,
  textStyle,
  disabled,
  testID,
  opacity,
}) => (
  <TouchableOpacity
    testID={testID}
    onPress={onPress}
    style={[styles.buttonStyle, style, {opacity}]}
    disabled={disabled}>
    <H3
      centered
      color={inverted ? colors.greyLight : colors.white}
      style={{...textStyle}}>
      {children}
    </H3>
  </TouchableOpacity>
);

Button.propTypes = {
  children: PropTypes.string,
  inverted: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
};

Button.defaultProps = {
  children: '',
  inverted: false,
};

const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: colors.blue,
    borderRadius: 5,
    borderWidth: 0,
    borderColor: colors.white,
    marginTop: 30,
    paddingLeft: 10,
    paddingRight: 10,
    alignSelf: 'stretch',
    height: 50,
    justifyContent: 'center',
    width: '100%',
    fontWeight: '700',
  },
  buttonStyleReversed: {
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: colors.white,
    marginTop: 30,
    paddingLeft: 10,
    paddingRight: 10,
    alignSelf: 'stretch',
    height: 50,
    justifyContent: 'center',
  },
  textStyle: {},
});

export {Button, styles};
