import React, {Component} from 'react';
import {StyleSheet, Dimensions, View, Platform} from 'react-native';
// import { tl } from '../../utils/i18n';
import {Button} from './Button';
import {H2, P1, colors} from '../../designSystem';

class NeedUpdate extends Component {
  onPress = () => {
    this.goToStore();
  };

  goToStore = () => {
    if (Platform.OS === 'ios') {
      this.goToAppStore();
    }
    if (Platform.OS === 'android') {
      this.goToPlayStore();
    }
  };

  goToAppStore = () => {
    alert('app store url');
  };

  //market://details?id=com.packagename
  goToPlayStore = () => {
    alert('play store url')
  };

  render() {
    return (
      <View style={styles.containerStyle}>
        <H2 centered>Need Update</H2>
        <P1 centered>You Need to update the app</P1>
        <Button onPress={this.onPress} style={{width: '100%'}}>
          dssd
        </Button>
      </View>
    );
  }
}
const {height} = Dimensions.get('window');

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: colors.greyDark,
    paddingHorizontal: 20,
    paddingVertical: height / 6,
  },
});

export {NeedUpdate};
