import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Touchable } from './Touchable';

const HeaderMenu = ({ icon ,onPress }) => {
  const { buttonStyle, menuStyle } = styles;

  return (
    <View style={menuStyle}>
      <Touchable onPressIn={onPress} style={buttonStyle}>
        {icon}
      </Touchable>
    </View>
  );
};

const styles = StyleSheet.create({
  menuStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal:15
  },
  buttonStyle: {
    width: 46,
    height: 46,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export { HeaderMenu };
