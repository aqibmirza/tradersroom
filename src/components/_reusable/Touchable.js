import React, { useEffect, useState } from 'react';
import { TouchableOpacity } from 'react-native';

const Touchable = props => {
  const [preventDoubleTap, setPreventDoubleTap] = useState(false);
  useEffect(() => {
    const timer = setTimeout(() => {
      if (preventDoubleTap) setPreventDoubleTap(false);
    }, 300);
    return () => clearTimeout(timer);
  }, [preventDoubleTap, setPreventDoubleTap]);
  const onPress = () => {
    setPreventDoubleTap(true);
    props.onPress();
  };
  return (
    <props.component {...props} disabled={preventDoubleTap || props.disabled} onPress={onPress}>
      {props.children}
    </props.component>
  );
};
Touchable.defaultProps = {
  onPress: () => {},
  component: TouchableOpacity
};
export { Touchable };
