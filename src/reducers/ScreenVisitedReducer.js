
const INITIAL_STATE = {
 
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SCREEN_VISITED':
      return { ...state, [action.payload]: true };
    default:
      return state;
  }
};
