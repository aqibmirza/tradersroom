import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';
import {Scene, Router, Reducer} from 'react-native-router-flux';

import {onBackAndroidHandler} from './utils/helpers';
import {SplashScreen} from './components/_reusable';
import {colors} from './designSystem';
import {
  ForgotPassword,
  LoginForm,
  SignupForm,
} from './modules/Authentication/ui';
import {Home} from './modules/Home/ui';
import {Profile} from './modules/Profile/ui';

const reducerCreate = params => {
  const defaultReducer = new Reducer(params);
  return (state, action) => {
    return defaultReducer(state, action);
  };
};

const RouterComponent = () => {
  // if(Platform.OS == 'android') StatusBar.setBackgroundColor(colors.orange);
  // StatusBar.setBarStyle('light-content');

  return (
    <Router
      createReducer={reducerCreate}
      backAndroidHandler={onBackAndroidHandler}
      titleStyle={{
        color: colors.white,
        fontWeight: null,
        fontSize: 20,
      }}
      sceneStyle={{backgroundColor: colors.white}}
      navigationBarStyle={styles.navBarStyle}
      tintColor={colors.white}
      headerForceInset={{top: 'never'}}>
      <Scene key="root" backTitle=" " headerLayoutPreset="center">
        <Scene key="loaderPlaceholder" component={SplashScreen} hideNavBar />
        {/* Start - General */}

        {/* End - General */}
        {/* Start - Auth */}
        <Scene key="login" component={LoginForm} hideNavBar />
        <Scene key="signup" component={SignupForm} hideNavBar />
        <Scene key="forgotpassword" component={ForgotPassword} hideNavBar />
        {/* End - Auth */}
        {/* Start - Home */}
        <Scene key="home" component={Home} hideNavBar />
        {/* End - Home */}
        {/* Start - Home */}
        <Scene key="profile" component={Profile} hideNavBar />
        {/* End - Home */}
      </Scene>
    </Router>
  );
};

const styles = StyleSheet.create({
  navBarStyle: {
    backgroundColor: colors.blue,
    height: 105,
    borderBottomLeftRadius: 40,
    borderBottomRightRadius: 40,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  splashimage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});

export default connect(null, {})(RouterComponent);
