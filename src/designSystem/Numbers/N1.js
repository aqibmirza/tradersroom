import React from 'react';
import PropTypes from 'prop-types';
import {Text} from 'react-native';
import {colors} from '../_styles/colors';

import {formatNumber} from '../../utils/helpers';

const N1 = props => {
  const {value, unit, color, opacity, decimals} = props;

  return (
    <Text>
      <Text style={{fontSize: 32, color, opacity}}>
        {formatNumber(value, decimals)}
      </Text>
      {unit && <Text style={{fontSize: 16, color, opacity}}>{` ${unit}`}</Text>}
    </Text>
  );
};

N1.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  unit: PropTypes.string,
  color: PropTypes.string,
  opacity: PropTypes.number,
  decimals: PropTypes.number,
};

N1.defaultProps = {
  value: null,
  unit: null,
  color: colors.white,
  opacity: 1,
  decimals: 2,
};

export {N1};
