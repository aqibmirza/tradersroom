const colors = {
  black: '#262626',
  blue: '#40BFFF',
  grey: '#C1C0C8',
  greyDark: '#CACACA',
  charcol: '#454545',
  white: '#FFFFFF',
  red: '#ED1C24',
  greyLight: '#41444A',
  orange: '#FF3131',
  transparent: 'transparent',
  green: '#24D041',
  maroon: '#F1592A',
  alizarin: '#F6222A',
  irisBlue: '#00ABD6',
  brightGray: '#EAEAEA'
  
};

export { colors };
