import addMilliseconds from 'date-fns/add_milliseconds';
import isPast from 'date-fns/is_past';
import AsyncStorageService from '../services/asyncStorage.service';

const cachePrefix = 'RUNNER_CACHE';

export async function getCache(method, params) {
  try {
    const { cacheConfig } = global;
    if (!cacheConfig || !cacheConfig[method]) return null;

    const key = { cachePrefix, method, params };
    const stringifiedKey = JSON.stringify(key);
    const cache = await AsyncStorageService.getItem(stringifiedKey);

    if (!cache) return null;
    if (cache.error) return null;
    const parsedCache = JSON.parse(cache);
    if (isPast(parsedCache.expiration)) return null;
    return parsedCache.result;
  } catch (e) {
    return null;
  }
}

export async function saveCache(method, params, result) {
  try {
    //If the method is not in the config file, then nothing is saved to cache
    const { cacheConfig } = global;
    if (!cacheConfig || !cacheConfig[method]) return;

    const expiration = addMilliseconds(new Date(), cacheConfig[method]);
    const key = { cachePrefix, method, params };
    const value = { expiration, result };
    const stringifiedKey = JSON.stringify(key);
    const stringifiedValue = JSON.stringify(value);

    await AsyncStorageService.saveItem({ key: stringifiedKey, value: stringifiedValue });
  } catch (e) {
    //Silent fail
  }
}

export async function clearCachedResults() {
  await AsyncStorageService.clearAllEntriesByPrefix(cachePrefix);
}
