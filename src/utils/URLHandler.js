import { Linking, Alert } from 'react-native';
import { tl } from '#/utils/i18n';

export const openUrl = url => {
  Linking.canOpenURL(url)
    .then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        Alert.alert(tl('helpMenu', 'errors.1'));
      }
    })
    .catch(() => Alert.alert(tl('helpMenu', 'errors.1')));
};
