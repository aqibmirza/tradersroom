import firebase from 'react-native-firebase';
import AsyncStorageService from '../services/asyncStorage.service';
import { inAppNotifyTrigger } from './InAppNotifyHandler';

export default class PushNotificationHandler {
  initializeNotifications = async (registerFunc, subscribedTopics) => {
    const enabled = await firebase.messaging().hasPermission();
    if (!enabled) {
      try {
        await firebase.messaging().requestPermission();
      } catch (error) {
        //User rejected permissions
        return;
      }
    }
    //this is executed only when enabled=true or successful requestPermission()
    await this.setupAndHandleNotifications(registerFunc, subscribedTopics);
  };

  saveTokenServerside = async (registerFunc, subscribedTopics) => {
    //this is executed only when enabled=true or successful requestPermission()
    await this.setupAndHandleNotifications(registerFunc, subscribedTopics);
  };

  registerToken = async (registerFunc, subscribedTopics) => {
    //only triggers an update to backend if the tomen isn't in local storage.
    //also notice that signOut action removes 'fcmToken' as well.
    // let fcmToken = await AsyncStorageService.getItem('fcmToken');
    // if (!fcmToken || !subscribedTopics) {
    try {
      const fcmToken = await firebase.messaging().getToken();
      await registerFunc({ fcmToken, subscribedTopics });
      await AsyncStorageService.saveItem({ key: 'fcmToken', value: fcmToken });
    } catch (error) {
      await AsyncStorageService.multiRemoveItem(['fcmToken']);
    }
    // }
  };

  setupAndHandleNotifications = async (registerFunc, subscribedTopics) => {
    const channel = new firebase.notifications.Android.Channel(
      // 'barry-channel',
      // 'Barry',
      // firebase.notifications.Android.Importance.Max
    ).setDescription('All notification from dsdsada');
    await this.registerToken(registerFunc, subscribedTopics);

    // Create the channel
    firebase.notifications().android.createChannel(channel);

    this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
      registerFunc({ fcmToken, subscribedTopics });
    });

    this.messageListener = firebase.messaging().onMessage(() => {
      // Process your message as required
    });

    //Handler when notification was triggered when App in foreground
    this.notificationListener = firebase.notifications().onNotification(notification => {
      // Process your notification as required
      this.showInAppNotification(notification);

      inAppNotifyTrigger(notification, true);
    });

    //Handler when notification was triggered when App in background
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        // Get information about the notification that was opened
        const { notification } = notificationOpen;

        inAppNotifyTrigger(notification, true);
      });

    //Handler when notification was triggered when App is closed
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      // App was opened by a notification
      // Get information about the notification that was opened
      const { notification } = notificationOpen;

      inAppNotifyTrigger(notification);
    }
  };

  showInAppNotification = notificationInput => {
    // notificationInput.android.setChannelId('barry-channel').android.setSmallIcon('ic_notification');
    notificationInput.ios.setBadge(0);
    firebase.notifications().displayNotification(notificationInput);
  };

  deinitializeNotifications = () => {
    // this.onTokenRefreshListener();
    // this.notificationListener();
    // this.notificationOpenedListener();
  };
}
