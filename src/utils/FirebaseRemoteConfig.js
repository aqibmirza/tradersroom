import firebase from 'react-native-firebase';

export default {
  async get(key, initialValue = false) {
    firebase.config().setDefaults({
      [key]: initialValue
    });

    try {
      await firebase.config().fetch();
      await firebase.config().activateFetched();
      const snapshot = await firebase.config().getValue(key);
      return snapshot.val();
    } catch (e) {
      return initialValue;
    }
  }
};
