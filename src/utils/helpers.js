import { Actions } from 'react-native-router-flux';
import {
  format,
  isValid,
  getYear,
  setMonth,
  getDaysInMonth,
  eachDay,
  startOfMonth,
  endOfMonth,
  isSameDay,
  isSameMonth,
  setHours
} from 'date-fns';
import Toast from 'react-native-root-toast';
// import enLocale from 'date-fns/locale/en';
// import daLocale from 'date-fns/locale/da';
import { Store } from '../Store';

export const displayToast = text => {
  if (typeof text !== 'string') return;
  Toast.show(text, {
    duration: Toast.durations.LONG,
    position: Toast.positions.BOTTOM,
    shadow: true,
    animation: true,
    backgroundColor: '#41444A',
    textColor: 'white',
    containerStyle: {
      borderRadius: 15,
      marginBottom: 20
    },
    textStyle: {
      fontSize: 12
    },
    hideOnPress: true,
    delay: 100
  });
};

export const formatNumber = (number, decimals = 2) => {
  let returnValue = '-';
  try {
    const fixedNumber = number.toFixed(decimals);

    //Prevent returning "-0"
    if (Number(fixedNumber) === 0) {
      returnValue = (0).toFixed(decimals).replace('.', ',');
    } else {
      returnValue = number.toFixed(decimals).replace('.', ',');
    }
  } catch (_e) {}
  return returnValue;
};

// const getLocale = () => {
//   let returnValue;
//   try {
//     const { currentLanguage } = Store.getState().localization;
//     const locales = { da: daLocale, en: enLocale };
//     returnValue = locales[currentLanguage];
//   } catch (_e) {}
//   return returnValue;
// };

// export const formatDate = (date, template = 'D. MMM YYYY', locale) => {
//   let returnValue = '-';
//   try {
//     const valid = isValid(new Date(date));
//     if (valid) {
//       returnValue = format(new Date(date), template, { locale: locale || getLocale() });
//     }
//   } catch (_e) {}
//   return returnValue;
// };

export const onBackAndroidHandler = () => {
  if (Actions.state.index === 0) {
    return false;
  }
  Actions.pop();
  return true;
};

export const getAvailableYears = () => {
  const years = [];
  for (let i = 2018; i <= getYear(new Date()); i += 1) {
    years.push(i);
  }
  return years;
};

export const getAvailableMonths = year => {
  const months = [];
  for (let i = 0; i <= 11; i += 1) {
    months.push(formatDate(setMonth(new Date(year, 0), i), 'MMM'));
  }
  return months;
};

export const getAvailableDays = date => {
  const days = [];
  for (let i = 0; i < getDaysInMonth(date); i += 1) {
    days.push(i + 1);
  }
  return days;
};

const calculateAngle = (x, y) => {
  const angle = (Math.atan2(y, x) * 180) / Math.PI;
  return angle;
};

const getCurrentQuarter = (centerPos, targetPos) => {
  if (targetPos.x >= centerPos.x) {
    if (targetPos.y < centerPos.y) {
      return 1;
    }
    if (targetPos.y >= centerPos.y) {
      return 2;
    }
  } else {
    if (targetPos.y < centerPos.y) {
      return 4;
    }
    if (targetPos.y >= centerPos.y) {
      return 3;
    }
  }
};

export const calculateRotationAngle = (endPos, centerPos, startPos) => {
  let angle =
    calculateAngle(endPos.x - centerPos.x, endPos.y - centerPos.y) -
    calculateAngle(startPos.x - centerPos.x, startPos.y - centerPos.y);

  if (getCurrentQuarter(centerPos, startPos) === 4 && getCurrentQuarter(centerPos, endPos) === 3) {
    angle -= 360;
  }
  if (getCurrentQuarter(centerPos, startPos) === 3 && getCurrentQuarter(centerPos, endPos) === 4) {
    angle += 360;
  }

  return angle;
};
export const emailValidationCheck = email => /\S+@\S+\.\S+/.test(email);

export const graphDot = () => {
  return '·';
};

export const formatDateHoursOrDot = date => {
  if (date.getHours() % 6 === 0 || date.getHours() === 23) {
    return formatDate(date, 'HH:mm');
  }
  if (date.getHours() % 3 === 0) {
    return graphDot();
  }
};

export const sortingOrder = ['CONSUMPTION', 'TARIFF', 'FEE', 'SUBSCRIPTION', 'BARRY'];

export const invertSign = number => {
  if (isNaN(number) || number === 0) return number;
  return number * -1;
};

export const capitalize = string => string.charAt(0).toUpperCase() + string.slice(1);

//"input" is an array of objects : {date: Date, value: Number}
export const addMissingHours = input => {
  if (input.length === 0 || input.length >= 23) return input;
  const output = [];
  const { date } = input[0];
  for (let h = 0; h < 24; h += 1) {
    const hour = setHours(date, h);
    const inputItem =
      input.filter(item => new Date(item.date).toString() === new Date(hour).toString()).pop() ||
      {};

    output.push({
      date: new Date(hour),
      value: inputItem.value || 0
    });
  }

  return output;
};

//"input" is an array of objects : {date: Date, value: Number}
export const addMissingDays = input => {
  if (input.length === 0) return input;
  const { date } = input[0];
  if (input.length === getDaysInMonth(date)) return input;

  const daysArray = eachDay(startOfMonth(date), endOfMonth(date));
  return daysArray.map(day => {
    const inputItem = input.filter(item => isSameDay(item.date, day)).pop() || {};

    return {
      date: new Date(day),
      value: inputItem.value || 0
    };
  });
};

//"input" is an array of objects : {date: Date, value: Number}
export const addMissingMonths = input => {
  if (input.length === 0 || input.length === 12) return input;
  const output = [];
  const { date } = input[0];
  for (let m = 0; m < 12; m += 1) {
    const month = setMonth(date, m);
    const inputItem = input.filter(item => isSameMonth(item.date, month)).pop() || {};

    output.push({
      date: new Date(month),
      value: inputItem.value || 0
    });
  }

  return output;
};

export const getShortAddress = address => {
  let returnValue = '';
  try {
    const splitAddress = address.split(',');
    switch (splitAddress.length) {
      case 1:
      case 2:
        returnValue = `${splitAddress[0]}`;
        break;
      case 3:
        returnValue = `${splitAddress[0]},${splitAddress[1]}`;
        break;
      default:
        break;
    }
  } catch (_e) {}
  return returnValue;
};

export const filterSupplyToGridMpids = inputArray => {
  let returnArray = [];
  let tempArray = [];
  try {
    //FILTER D06 (SUPPLY_TO_GRID) MPID TYPE
    tempArray = JSON.parse(JSON.stringify(inputArray));
    returnArray.push(
      ...tempArray
        .map(({ mpid, type, subType, childMpids }) => [{ mpid, type, subType }, childMpids])
        .flat(Infinity)
        .filter(({ type }) => type === 'D06')
        .map(({ mpid }) => mpid)
    );
  } catch (_e) {
    returnArray = [];
  }

  return returnArray;
};
