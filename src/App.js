import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import SafeAreaView from 'react-native-safe-area-view';

import Initializer from './Initializer';
import Router from './Router';
import {Store, Persistor} from './Store';
import {Loading, StatusBarColor} from './components/_reusable';
import {colors} from './designSystem';

// LogBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Require cycle:']);
export default () => (
  <SafeAreaProvider>
    <Provider store={Store}>
      <PersistGate loading={<Loading />} persistor={Persistor}>
        <Initializer>
          <StatusBarColor
            backgroundColor={colors.blue}
            barStyle="light-content"
          />
          <SafeAreaView
            style={{flex: 1, backgroundColor: colors.transparent}}
            forceInset={{bottom: 'never'}}>
            <Router />
          </SafeAreaView>
        </Initializer>
      </PersistGate>
    </Provider>
  </SafeAreaProvider>
);
