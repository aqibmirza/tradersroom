import React, { useRef, useState } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, ActivityIndicator, View, TextInput, StatusBar, ImageBackground, Dimensions } from 'react-native';
import { Button, Input, Loading } from '#/components/_reusable';
// import { tl } from '#/utils/i18n';
import { colors, H0, P1, H5,P4 } from '#/designSystem';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Envelope from '#/resources/img/svg/Envelope';
import { goToLogin } from '#/modules/Authentication/actions';

const { height, width } = Dimensions.get('window');

const ForgotPassword = props => {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const disabled = !email

  const onSubmit = () => {
    if (disabled) return;
    
    setLoading(true);
    setError(null);
    // try {
    //   // await props.loginCprFlow(cpr);
    // } catch (e) {
    //   setError(e.message);
    // }
    setLoading(false);
  };
  const renderButton = () => {
    if (loading) return <Loading transparent />;
    return (
      <Button disabled={disabled} onPress={onSubmit} opacity={!disabled ? 1 : 0.5 } style={{marginTop:10}}>
        Submit
      </Button>
    );
  };

  return (
    <ImageBackground source={{ uri: "loginbg" }} style={styles.image}>
      <KeyboardAwareScrollView contentContainerStyle={styles.screenContainerStyle}>
        <View style={styles.loginBox}>
        <H0 centered>Forgot Password</H0>
        <P1 centered style={{ marginBottom: 30 }}>Enter your email to get your password</P1>
        <View>
          <Input
            onChangeText={(value)=> setEmail(value)}
            value={email}
            onSubmitEditing={onSubmit}
            error={error}
            keyboardType="email-address"
            placeholder="Your Email"
            autoCompleteType="email"
            returnKeyType="next"
            icon={<Envelope/>}
            styleInput={{color:colors.black}}
          />
        </View>
        <View>{renderButton()}</View>
        <View style={{flexDirection:'row',justifyContent:'center'}}>
        <H5 style={{marginVertical:10,height:20}} color={colors.blue} onPress={() => props.goToLogin()}>Back to Login</H5>
        </View>
        </View>
      </KeyboardAwareScrollView>

    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  screenContainerStyle:{
    flexGrow:1,
    justifyContent:"space-between"
  },
  image: {
    flex: 1,
    resizeMode: "cover"
  },
  loginBox:{
    backgroundColor:colors.white,
    paddingTop:20,
    paddingHorizontal:38,
    borderTopLeftRadius:26,
    borderTopRightRadius:26,
    paddingBottom:60
  },
  forgotPassword:{
    textAlign:'right',
    marginVertical:10
  },
  registerText:{
    marginTop:10,
    marginBottom:60
  }
});

const ConnectedForgotPassword = connect(null, {
  goToLogin
})(ForgotPassword);

export { ConnectedForgotPassword as ForgotPassword };
