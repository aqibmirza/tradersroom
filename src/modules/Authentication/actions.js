import {Actions} from 'react-native-router-flux';
import {NAVIGATE_TO_SCREEN} from '../../actions/types';

export const goToLogin = () => dispatch => {
  dispatch({type: NAVIGATE_TO_SCREEN, payload: 'LOGIN'});
  Actions.login();
};

export const goToSignup = () => dispatch => {
  dispatch({type: NAVIGATE_TO_SCREEN, payload: 'SIGNUP'});
  Actions.signup();
};
export const goToForgotPassword = () => dispatch => {
  dispatch({type: NAVIGATE_TO_SCREEN, payload: 'FORGOT_PASSWORD'});
  Actions.forgotpassword();
};
