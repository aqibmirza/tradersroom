import { NAVIGATE_TO_SCREEN } from "#/actions/types";
import { Actions,ActionConst } from 'react-native-router-flux';


export const goToProfile = () => dispatch => {
    dispatch({ type: NAVIGATE_TO_SCREEN, payload: 'PROFILE' });
    Actions.profile({type : ActionConst.RESET});
};