import { NAVIGATE_TO_SCREEN } from "#/actions/types";
import { Actions,ActionConst } from 'react-native-router-flux';


export const goToHome = () => dispatch => {
    dispatch({ type: NAVIGATE_TO_SCREEN, payload: 'HOME' });
    Actions.home({type : ActionConst.RESET});
};