import React from 'react';
import Svg, { Path } from "react-native-svg"

const Humburger = props => (
  <Svg width={20} height={16} viewBox="0 0 20 16" fill="none" {...props}>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M19 0c.6 0 1 .457 1 1.143 0 .686-.4 1.143-1 1.143H1c-.6 0-1-.457-1-1.143C0 .457.4 0 1 0h18zM5 6.857h14c.6 0 1-.457 1-1.143 0-.685-.4-1.143-1-1.143H5c-.6 0-1 .458-1 1.143 0 .686.4 1.143 1 1.143zm15 3.429c0-.686-.4-1.143-1-1.143H1c-.6 0-1 .457-1 1.143 0 .685.4 1.143 1 1.143h18c.6 0 1-.458 1-1.143zm-1 3.428c.6 0 1 .457 1 1.143 0 .686-.4 1.143-1 1.143H5c-.6 0-1-.457-1-1.143 0-.686.4-1.143 1-1.143h14z"
        fill="#fff"
      />
    </Svg>
);

export default Humburger;
