import React from 'react';
import { Svg, Path } from 'react-native-svg';

const Arrow = props => {
  let h;
  let w;
  let d;
  let vh;
  let vw;
  switch (props.direction) {
    case 'down':
      h = props.height || 8;
      w = props.width || 14;
      vh = 8;
      vw = 14;
      d = 'M1.5 1.5L7 6.5L12.5 1.5';
      break;
    case 'up':
      h = props.height || 8;
      w = props.width || 14;
      vh = 8;
      vw = 14;
      d = 'M12.5 6.5L7 1.5L1.5 6.5';
      break;
    case 'left':
      h = props.height || 14;
      w = props.width || 8;
      vh = 14;
      vw = 8;
      d = 'M6.5 1.5L1.5 7L6.5 12.5';
      break;
    case 'right':
    default:
      h = props.height || 14;
      w = props.width || 8;
      vh = 14;
      vw = 8;
      d = 'M1.5 12.5L6.5 7L1.5 1.5';
      break;
  }
  return (
    <Svg width={w} height={h} viewBox={`0 0 ${vw} ${vh}`} fill="none">
      <Path
        d={d}
        stroke={props.fill || '#FFF'}
        strokeOpacity={props.opacity || 0.5}
        strokeWidth={2}
        strokeLinecap="round"
      />
    </Svg>
  );
};

export default Arrow;
