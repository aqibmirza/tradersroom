import React from 'react';
import Svg, { Path } from 'react-native-svg';

const Check = props => (
  <Svg width={16} height={16} fill="none" {...props}>
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M4.939.609A8 8 0 0 1 8 0c4.4 0 8 3.576 8 8A8 8 0 1 1 4.939.609zm7.846 4.702a.794.794 0 0 0 0-1.086.711.711 0 0 0-1.038 0L6.395 9.827 4.253 7.714a.71.71 0 0 0-1.038 0 .794.794 0 0 0 0 1.086l3.18 3.2 6.39-6.689z"
      fill="#fff"
    />
  </Svg>
);

export default Check;
